import React from 'react'
import {BrowserRouter, Redirect, Route, Switch} from 'react-router-dom'
import {withSuspense} from './hoc/withSuspense'
import {initializeApp} from './redux/app/appReducer'
import {compose} from 'redux'
import {connect, Provider} from 'react-redux'
import store, {AppStateType} from './redux/reduxStore'

import './App.css'

import UsersContainer from './components/Users/UsersContainer'
import HeaderContainer from './components/Header/HeaderContainer'
import LoginPage from './components/Login/Login'
import Preloader from './components/common/Preloader/Preloader'
import {Col, Layout} from 'antd'
import NavbarContainer from './components/Navbar/NavbarContainer'

const DialogsContainer = React.lazy(() => import('./components/Dialogs/DialogsContainer'))
const ProfileContainer = React.lazy(() => import('./components/Profile/ProfileContainer'))
const ChatPage = React.lazy(()=> import ('./pages/Chat/ChatPage'))

type MapPropsType = ReturnType<typeof mapStateToProps>

type DispatchPropsType = {
    initializeApp: () => void
}

type PropsType = MapPropsType & DispatchPropsType


const SuspendedProfilePage = withSuspense(ProfileContainer)
const SuspendedDialogsPage = withSuspense(DialogsContainer)
const SuspendedChatPage = withSuspense(ChatPage)

const {Content} = Layout

class App extends React.Component<PropsType> {
    catchAllUnhandledErrors = (e: PromiseRejectionEvent) => {
        console.error('Some error occurred')
        console.error(e)
    }

    componentDidMount() {
        this.props.initializeApp()
        window.addEventListener('unhandledrejection', this.catchAllUnhandledErrors)
    }

    componentWillUnmount() {
        window.removeEventListener('unhandledrejection', this.catchAllUnhandledErrors)
    }

    render() {
        if (!this.props.initialized) {
            return <Preloader/>
        }

        return (
            <div className='app-wrapper'>
                <Layout>
                    <HeaderContainer/>
                </Layout>
                <Content className={'app-wrapper__container'}>
                    <Col span={20} offset={4}>
                        <Layout style={{backgroundColor: 'black'}}>
                            <NavbarContainer/>
                            <Col span={16}>
                                <Content className={'app-wrapper__content'}>
                                    <Switch>
                                        <Route exact path='/'
                                               render={() => <Redirect to={'/profile'}/>} />

                                        <Route path='/dialogs'
                                               render={() => <SuspendedDialogsPage/>} />

                                        <Route path='/profile/:userId?'
                                               render={() => <SuspendedProfilePage/>} />

                                        <Route path='/users'
                                               render={() => <UsersContainer/>} />

                                        <Route path='/login'
                                               render={() => <LoginPage/>} />

                                        <Route path='/chat'
                                               render={() => <SuspendedChatPage/>} />

                                        <Route path='*'
                                               render={() => <div>404 NOT FOUND</div>} />

                                    </Switch>
                                </Content>
                            </Col>
                        </Layout>
                    </Col>

                </Content>

            </div>
        )
    }
}

const mapStateToProps = (state: AppStateType) => ({
    initialized: state.app.initialized
})

let AppContainer = compose<React.ComponentType>(
    connect(mapStateToProps, {initializeApp})
)(App)

const BestApp: React.FC = () => {
    return <BrowserRouter>
        <Provider store={store}>
            <AppContainer/>
        </Provider>
    </BrowserRouter>
}

export default BestApp
