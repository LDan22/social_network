import React from 'react'
import ReactDOM from 'react-dom'
import BestApp from './App'


test('renders without crashing', () => {
    const div = document.createElement('div')
    ReactDOM.render(<BestApp/>, div)
    ReactDOM.unmountComponentAtNode(div)
})
