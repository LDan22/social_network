export const updateObjectInArray = (items: any, value: any, objPropName: any, newObjProps: any) => {
    return items.map((i: any) => {
        if (i[objPropName] === value) {
            return {...i, ...newObjProps}
        }
        return i
    })
}
