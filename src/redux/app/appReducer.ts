import {getAuthUserData} from '../auth/authReducer'
import {BaseThunkType, InferActionsTypes} from '../reduxStore'

export const INITIALIZED_SUCCESS = 'network/app/INITIALIZED_SUCCESS'

let initialState = {
    initialized: false
}

export type InitialStateType = typeof initialState

const appReducer = (state = initialState, action: ActionsType): InitialStateType => {
    switch (action.type) {
        case INITIALIZED_SUCCESS:
            return {
                ...state,
                initialized: true
            }

        default:
            return state
    }
}

export const actions = {
    initializedSuccess: () => ({type: INITIALIZED_SUCCESS} as const)
}

export type ActionsType = InferActionsTypes<typeof actions>
type ThunkType = BaseThunkType<ActionsType>

export const initializeApp = (): ThunkType => async (dispatch) => {
    let promise = dispatch(getAuthUserData())

    Promise.all([promise])
        .then(() => {
            dispatch(actions.initializedSuccess())
        })
}


export default appReducer
