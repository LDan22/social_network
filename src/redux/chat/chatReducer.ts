import {BaseThunkType, InferActionsTypes} from '../reduxStore'
import {chatAPI, ChatMessageType, StatusType} from '../../api/chatAPI'
import {Dispatch} from 'redux'
import {v1} from 'uuid'

export const MESSAGES_RECEIVED = 'network/chat/MESSAGES_RECEIVED'
export const STATUS_CHANGED = 'network/chat/STATUS_CHANGED'

let initialState = {
    messages: [] as ChatMessageIdentifiableType[],
    status: 'pending' as StatusType
}

type Identifiable = {
    id: string
}

export type ChatMessageIdentifiableType = ChatMessageType & Identifiable

export type InitialStateType = typeof initialState

const chatReducer = (state = initialState, action: ActionsType): InitialStateType => {
    switch (action.type) {
        case MESSAGES_RECEIVED:
            return {
                ...state,
                messages: [...state.messages, ...action.payload.messages.map(m => ({...m, id: v1()}))]
                    .filter((m, index, array) => index >= array.length - 100)
            }

        case STATUS_CHANGED:
            return {
                ...state,
                status: action.payload
            }

        default:
            return state
    }
}

export const actions = {
    messagesReceived: (messages: ChatMessageType[]) => ({type: MESSAGES_RECEIVED, payload: {messages}} as const),
    statusChanged: (status: StatusType) => ({type: STATUS_CHANGED, payload: status} as const)
}

export type ActionsType = InferActionsTypes<typeof actions>
type ThunkType = BaseThunkType<ActionsType>

let _newMessageHandler: ((messages: ChatMessageType[]) => void) | null = null

const newMessageHandler = (dispatch: Dispatch) => {
    if (_newMessageHandler === null) {
        _newMessageHandler = (messages) => {
            dispatch(actions.messagesReceived(messages))
        }
    }
    return _newMessageHandler
}

let _newStatusChangedHandler: ((status: StatusType) => void) | null = null

const newStatusChangeHandler = (dispatch: Dispatch) => {
    if (_newStatusChangedHandler === null) {
        _newStatusChangedHandler = (status) => {
            dispatch(actions.statusChanged(status))
        }
    }
    return _newStatusChangedHandler
}


export const startMessagesListening = (): ThunkType => async (dispatch) => {
    chatAPI.start()
    chatAPI.subscribe('message-received', newMessageHandler(dispatch))
    chatAPI.subscribe('status-changed', newStatusChangeHandler(dispatch))
}

export const stopMessagesListening = (): ThunkType => async (dispatch) => {
    chatAPI.unsubscribe('message-received', newMessageHandler(dispatch))
    chatAPI.unsubscribe('status-changed', newStatusChangeHandler(dispatch))
    chatAPI.stop()
}

export const sendMessage = (message: string): ThunkType => async (dispatch) => {
    chatAPI.sendMessage(message)
}

export default chatReducer
