import {updateObjectInArray} from '../../utils/objectHelpers'
import {UserType} from '../../types/types'
import {Dispatch} from 'redux'
import {BaseThunkType, InferActionsTypes} from '../reduxStore'
import {usersAPI} from '../../api/usersAPI'
import {APIResponseType, ResultCodes} from '../../api/api'

export const FOLLOW = 'network/users/FOLLOW'
export const UNFOLLOW = 'network/users/UNFOLLOW'
export const SET_USERS = 'network/users/SET_USERS'
export const SET_CURRENT_PAGE = 'network/users/SET_CURRENT_PAGE'
export const SET_TOTAL_USERS_COUNT = 'network/users/SET_TOTAL_USERS_COUNT'
export const TOGGLE_IS_FETCHING = 'network/users/TOGGLE_IS_FETCHING'
export const TOGGLE_IS_FOLLOWING_PROGRESS = 'network/users/TOGGLE_IS_FOLLOWING_PROGRESS'
export const SET_FILTER = 'network/users/SET_FILTER'

let initialState = {
    users: [] as Array<UserType>,
    pageSize: 5,
    totalUserCount: 0,
    currentPage: 1,
    isFetching: false,
    followingInProgress: [] as Array<number>, // array of userIds
    filter: {
        term: '',
        friend: null as null | boolean
    }
}

export type InitialStateType = typeof initialState
export type FilterType = typeof initialState.filter

const usersReducer = (state = initialState, action: ActionsTypes): InitialStateType => {
    switch (action.type) {
        case FOLLOW:
            return {
                ...state,
                users: updateObjectInArray(state.users, action.userId, 'id', {followed: true})
            }

        case UNFOLLOW:
            return {
                ...state,
                users: updateObjectInArray(state.users, action.userId, 'id', {followed: false})
            }

        case SET_USERS:
            return {
                ...state,
                users: action.users
            }

        case SET_CURRENT_PAGE:
            return {
                ...state,
                currentPage: action.currentPage
            }

        case SET_TOTAL_USERS_COUNT:
            return {
                ...state,
                totalUserCount: action.totalUserCount
            }

        case TOGGLE_IS_FETCHING:
            return {
                ...state,
                isFetching: action.isFetching
            }

        case TOGGLE_IS_FOLLOWING_PROGRESS:
            return {
                ...state,
                followingInProgress: action.isFetching
                    ? [...state.followingInProgress, action.userId]
                    : [...state.followingInProgress.filter(id => id !== action.userId)]

            }
        case SET_FILTER:
            return {
                ...state,
                filter: action.payload
            }
        default:
            return state
    }
}

// action creators

export const actions = {
    followSuccess: (userId: number) => ({type: FOLLOW, userId} as const),
    unfollowSuccess: (userId: number) => ({type: UNFOLLOW, userId} as const),
    setUsers: (users: Array<UserType>) => ({type: SET_USERS, users} as const),
    setCurrentPage: (page: number) => ({type: SET_CURRENT_PAGE, currentPage: page} as const),
    setFilter: (filter: FilterType) => ({type: SET_FILTER, payload: filter} as const),
    setTotalUsersCount: (totalUserCount: number) => ({type: SET_TOTAL_USERS_COUNT, totalUserCount} as const),
    toggleIsFetching: (isFetching: boolean) => ({type: TOGGLE_IS_FETCHING, isFetching} as const),
    toggleFollowingProgress: (userId: number, isFetching: boolean) => ({
        type: TOGGLE_IS_FOLLOWING_PROGRESS,
        userId,
        isFetching
    } as const)
}

type ActionsTypes = InferActionsTypes<typeof actions>

// thunk creators
type DispatchType = Dispatch<ActionsTypes>
type ThunkType = BaseThunkType<ActionsTypes>


export const requestUsers = (page: number, pageSize: number, filter: FilterType): ThunkType =>
    async (dispatch) => {
        dispatch(actions.toggleIsFetching(true))
        dispatch(actions.setCurrentPage(page))
        dispatch(actions.setFilter(filter))

        let data = await usersAPI.getUsers(page, pageSize, filter.term, filter.friend)

        dispatch(actions.toggleIsFetching(false))
        dispatch(actions.setUsers(data.items))
        dispatch(actions.setTotalUsersCount(data.totalCount))
    }

const _followUnfollowFlow = async (dispatch: DispatchType, userId: number,
                                   apiMethod: (userId: number) => Promise<APIResponseType>,
                                   actionCreator: (userId: number) => ActionsTypes) => {

    dispatch(actions.toggleFollowingProgress(userId, true))

    let data = await apiMethod(userId)

    if (data.resultCode === ResultCodes.SUCCESS) {
        dispatch(actionCreator(userId))
    }

    dispatch(actions.toggleFollowingProgress(userId, false))
}

export const follow = (userId: number): ThunkType =>
    async (dispatch) => {
        await _followUnfollowFlow(dispatch, userId, usersAPI.follow.bind(usersAPI), actions.followSuccess)
    }

export const unfollow = (userId: number): ThunkType =>
    async (dispatch) => {
        await _followUnfollowFlow(dispatch, userId, usersAPI.unfollow.bind(usersAPI), actions.unfollowSuccess)
    }


export default usersReducer
