import {createSelector} from 'reselect'
import {AppStateType} from '../reduxStore'

export const getUsers = (state: AppStateType) => {
    return state.usersPage.users
}

export const getFilteredUsers = createSelector(getUsers, (users) => {
    return users.filter(u => true)
})

export const getPageSize = (state: AppStateType) => {
    return state.usersPage.pageSize
}

export const getTotalUserCount = (state: AppStateType) => {
    return state.usersPage.totalUserCount
}

export const getCurrentPage = (state: AppStateType) => {
    return state.usersPage.currentPage
}

export const getIsFetchingStatus = (state: AppStateType) => {
    return state.usersPage.isFetching
}

export const getFollowingInProgressStatus = (state: AppStateType) => {
    return state.usersPage.followingInProgress
}

export const getUsersFilter = (state: AppStateType) => {
    return state.usersPage.filter
}
