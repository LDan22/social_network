import {FormAction, reset} from 'redux-form'
import {BaseThunkType, InferActionsTypes} from '../reduxStore'

export const ADD_MESSAGE = 'network/dialogs/ADD_MESSAGE'

type Message = {
    id: number,
    message: string,
}

type Dialog = {
    id: number,
    name: string
}

const initialState = {
    messages: [
        {id: 1, message: 'Hello'},
        {id: 2, message: 'How are you'},
        {id: 3, message: 'Cool!'}
    ] as Array<Message>,
    dialogs: [
        {id: 1, name: 'Dimych'},
        {id: 2, name: 'Dan'},
        {id: 3, name: 'John'},
        {id: 4, name: 'Andrey'},
        {id: 5, name: 'Max'},
        {id: 6, name: 'Mike'}
    ] as Array<Dialog>,
    newMessageText: ''
}

export type InitialStateType = typeof initialState
type ActionsType = InferActionsTypes<typeof actions>
type ThunkType = BaseThunkType<ActionsType | FormAction>


const dialogsReducer = (state = initialState, action: ActionsType): InitialStateType => {
    switch (action.type) {
        case ADD_MESSAGE:
            let lastID = state.messages[state.messages.length - 1].id

            let newMessage = {
                id: lastID + 1,
                message: action.newMessageText
            }

            return {
                ...state,
                messages: [...state.messages, newMessage],
                newMessageText: ''
            }

        default:
            return state
    }

}

const actions = {
    saveMessage: (newMessageText: string) => ({
        type: ADD_MESSAGE,
        newMessageText: newMessageText
    } as const)
}


export const addMessage = (newMessageText: string): ThunkType =>
    async (dispatch) => {
        dispatch(actions.saveMessage(newMessageText))
        dispatch(reset('dialogAddMessageForm'))
    }

export default dialogsReducer
