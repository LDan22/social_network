import {APIResponseType, ResultCodeForCaptcha, ResultCodes} from '../../api/api'
import {FormAction, stopSubmit} from 'redux-form'
import authAPI from '../../api/authAPI'
import {securityAPI} from '../../api/securityAPI'
import {BaseThunkType, InferActionsTypes} from '../reduxStore'

export const SET_USER_DATA = 'network/auth/SET_USER_DATA'
export const SET_CAPTCHA_URL = 'network/auth/SET_CAPTCHA_URL'


let initialState = {
    userId: null as number | null,
    email: null as string | null,
    login: null as string | null,
    isFetching: false,
    isAuth: false,
    captchaUrl: null as string | null
}

export type InitialStateType = typeof initialState
export type ActionsType = InferActionsTypes<typeof actions>
type ThunkType = BaseThunkType<ActionsType | FormAction>


const authReducer = (state = initialState, action: ActionsType): InitialStateType => {
    switch (action.type) {
        case SET_USER_DATA:
            return {
                ...state,
                ...action.payload
            }
        case SET_CAPTCHA_URL:
            return {
                ...state,
                captchaUrl: action.payload.captchaUrl
            }
        default:
            return state
    }
}

// action creators

const actions = {
    setAuthUserData:
        (userId: number | null, email: string | null, login: string | null, isAuth: boolean) => ({
            type: SET_USER_DATA,
            payload: {userId, email, login, isAuth}
        } as const),
    setCaptchaUrl: (captchaUrl: string) => ({type: SET_CAPTCHA_URL, payload: {captchaUrl}} as const)
}


// thunks
export const getAuthUserData = (): ThunkType => async (dispatch) => {
    let data = await authAPI.me()

    if (data.resultCode === 0) {
        let {id, email, login} = data.data
        dispatch(actions.setAuthUserData(id, email, login, true))
    }
}

const _getFormError = (data: APIResponseType) => {
    let formError
    data.fieldsErrors && data.fieldsErrors.length > 0 ?
        formError = data.fieldsErrors.reduce((memo: any, value: any) =>
            Object.assign(memo, {[value.field]: value.error}), {})
        :
        (data.messages && data.messages.length > 0 ?
            formError = {_error: data.messages[0]} : formError = {_error: 'Some error occurred'})

    return formError
}

export const login =
    (email: string, password: string, rememberMe: boolean, captcha: string): ThunkType => async (dispatch) => {
        let data = await authAPI.login(email, password, rememberMe, captcha)

        if (data.resultCode === ResultCodes.SUCCESS) {
            await dispatch(getAuthUserData())
        } else {
            dispatch(stopSubmit('login', _getFormError(data)))

            if (data.resultCode === ResultCodeForCaptcha.CAPTCHA_IS_REQUIRED) {
                await dispatch(getCaptchaUrl())
            }
        }
    }

export const logout = (): ThunkType => async (dispatch) => {
    let data = await authAPI.logout()

    if (data.resultCode === ResultCodes.SUCCESS) {
        dispatch(actions.setAuthUserData(null, null, null, false))
    }
}

export const getCaptchaUrl = (): ThunkType => async (dispatch) => {
    let data = await securityAPI.getCaptcha()
    dispatch(actions.setCaptchaUrl(data.url))
}


export default authReducer
