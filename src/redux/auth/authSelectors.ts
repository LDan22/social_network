import {AppStateType} from '../reduxStore'

export const getIsAuthStatus = (state: AppStateType) => {
    return state.auth.isAuth
}
