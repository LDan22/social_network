import {Action, applyMiddleware, combineReducers, compose, createStore} from 'redux'
import profileReducer from './profile/profileReducer'
import dialogsReducer from './dialogs/dialogsReducer'
import sidebarReducer from './sidebar/sidebarReducer'
import usersReducer from './users/usersReducer'
import authReducer from './auth/authReducer'
import appReducer from './app/appReducer'

import {reducer as formReducer} from 'redux-form'

import thunkMiddleware, {ThunkAction} from 'redux-thunk'
import chatReducer from './chat/chatReducer'

let rootReducer = combineReducers({
    profilePage: profileReducer,
    dialogsPage: dialogsReducer,
    sidebar: sidebarReducer,
    usersPage: usersReducer,
    auth: authReducer,
    app: appReducer,
    form: formReducer,
    chat: chatReducer
})

type RootReducerType = typeof rootReducer;
export type AppStateType = ReturnType<RootReducerType>

export type InferActionsTypes<T> = T extends { [key: string]: (...args: any[]) => infer U } ? U : never
export type BaseThunkType<A extends Action, R = Promise<void>> = ThunkAction<R, AppStateType, unknown, A>


// @ts-ignore
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const store = createStore(rootReducer, composeEnhancers(
    applyMiddleware(thunkMiddleware)
))


export default store
