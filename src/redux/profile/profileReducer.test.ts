import profileReducer, {actions} from "./profileReducer";
import {PostType, ProfileType} from '../../types/types'

let state = {
    posts: [{id: 1, message: 'Hi, how are you?', likesCount: 10, added: 'Feb 4'},
        {id: 2, message: 'Hello, it is my first post', likesCount: 12, added: 'Jan 31'},
        {id: 3, message: 'My name is Dan', likesCount: 5, added: 'Jan 28'}] as Array<PostType>,
    profile: null as ProfileType | null,
    status: '',
    newPostText: ''
}

test('len of posts should be incremented', () => {
    let action = actions.addPost('test post')
    let newState = profileReducer(state, action);

    expect(newState.posts.length).toBe(4)
})

test('message of new post should be correct', () => {
    let action = actions.addPost('test post')
    let newState = profileReducer(state, action);

    expect(newState.posts[3].message).toBe('test post')
})

test('len of posts should be decremented after deleting', () => {
    let action = actions.deletePost(2)
    let newState = profileReducer(state, action);

    expect(newState.posts.length).toBe(2)
})

test('do not decrement posts length if id is incorrect', () => {
    let action = actions.deletePost(5)
    let newState = profileReducer(state, action);

    expect(newState.posts.length).toBe(3)
})
