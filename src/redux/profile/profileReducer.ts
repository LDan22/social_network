import {FormAction, stopSubmit} from 'redux-form'
import {PhotosType, PostType, ProfileType} from '../../types/types'
import {profileAPI} from '../../api/profileAPI'
import {ResultCodes} from '../../api/api'
import {BaseThunkType, InferActionsTypes} from '../reduxStore'

export const ADD_POST = 'network/profile/ADD_POST'
export const SET_USER_PROFILE = 'network/profile/SET_USER_PROFILE'
export const SET_STATUS = `network/profile/SET_STATUS`
export const DELETE_POST = 'network/profile/DELETE_POST'
export const SAVE_PROFILE_PHOTO = 'network/profile/SAVE_PHOTO'
export const UPDATE_PROFILE = 'network/profile/UPDATE_PROFILE'


let initialState = {
    posts: [{id: 1, message: 'Hi, how are you?', likesCount: 10, added: 'Feb 4'},
        {id: 2, message: 'Hello, it is my first post', likesCount: 12, added: 'Jan 31'},
        {id: 3, message: 'My name is Dan', likesCount: 5, added: 'Jan 28'}] as Array<PostType>,
    profile: null as ProfileType | null,
    status: '',
    newPostText: ''
}

export type InitialStateType = typeof initialState
type ActionsType = InferActionsTypes<typeof actions>
type ThunkType = BaseThunkType<ActionsType | FormAction>

const profileReducer = (state = initialState, action: ActionsType): InitialStateType => {
    switch (action.type) {
        case ADD_POST:
            let lastID = state.posts[state.posts.length - 1].id

            let newPost = {
                id: lastID + 1,
                message: action.newPostText,
                likesCount: 0,
                added: 'recently'
            }

            return {
                ...state,
                posts: [...state.posts, newPost],
                newPostText: ''
            }

        case SET_USER_PROFILE:
            return {
                ...state,
                profile: action.profile
            }

        case SET_STATUS: {
            return {
                ...state,
                status: action.status
            }
        }

        case DELETE_POST:
            return {
                ...state,
                posts: state.posts.filter(p => p.id !== action.payload)
            }

        case SAVE_PROFILE_PHOTO:
            return {
                ...state,
                profile: {
                    ...state.profile,
                    photos: action.payload
                } as ProfileType
            }
        default:
            return state
    }
}

// action creators

export const actions = {
    addPost: (newPostText: string) => ({type: ADD_POST, newPostText} as const),
    deletePost: (postId: number) => ({type: DELETE_POST, payload: postId} as const),
    setUserProfile: (profile: ProfileType) => ({type: SET_USER_PROFILE, profile} as const),
    setStatus: (status: string) => ({type: SET_STATUS, status} as const),
    savePhotoSuccess: (photos: PhotosType) => ({type: SAVE_PROFILE_PHOTO, payload: photos} as const)
}

// thunks
export const getUserProfile = (userId: number): ThunkType => async (dispatch) => {
    let data = await profileAPI.getProfile(userId)
    console.log(data)
    dispatch(actions.setUserProfile(data))
}

export const getUserStatus = (userId: number): ThunkType => async (dispatch) => {
    let data = await profileAPI.getStatus(userId)
    dispatch(actions.setStatus(data))
}

export const updateStatus = (status: string): ThunkType => async (dispatch) => {
    try {
        let data = await profileAPI.updateStatus(status)

        if (data.resultCode === ResultCodes.SUCCESS) {
            dispatch(actions.setStatus(status))
        }
    } catch (e) {
        console.log(e)
    }
}

export const savePhoto = (photo: File): ThunkType => async (dispatch) => {
    let data = await profileAPI.savePhoto(photo)

    if (data.resultCode === ResultCodes.SUCCESS) {
        dispatch(actions.savePhotoSuccess(data.data.photos))
    }
}

export const updateProfile = (profile: ProfileType): ThunkType => async (dispatch, getState) => {
    let data = await profileAPI.updateProfile(profile)
    const userId = getState().auth.userId

    if (data.resultCode === ResultCodes.SUCCESS) {
        if (userId !== null)
            await dispatch(getUserProfile(userId))
    } else {
        dispatch(stopSubmit('profileDataForm', {_error: data.messages[0]}))
        return Promise.reject(data.messages[0])
    }
}

export default profileReducer
