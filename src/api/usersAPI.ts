import {APIResponseType, GetItemsType, instance} from './api'
import {UserType} from '../types/types'


export const usersAPI = {
    getUsers(currentPage = 1, pageSize = 10, term: string, friend: null | boolean = null) {
        return instance
            .get<GetItemsType<UserType>>(`users?page=${currentPage}&count=${pageSize}&term=${term}`
                + (friend === null ? '' : `&friend=${friend}`))
            .then(res => res.data)
    },

    unfollow(userId: number) {
        return instance
            .delete<APIResponseType>(`follow/${userId}`)
            .then(res => {
                return res.data
            })
    },

    follow(userId: number) {
        return instance
            .post<APIResponseType>(`follow/${userId}`, {})
            .then(res => {
                return res.data
            })
    }
}
