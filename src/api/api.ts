import axios from 'axios'

let baseURL = 'https://social-network.samuraijs.com/api/1.0/'

export const instance = axios.create({
    withCredentials: true,
    baseURL: baseURL,
    headers: {
        'API-KEY': 'bb2d6d3b-4678-4be8-b876-c6cdf26b846a'
    }
})

export enum ResultCodes {
    SUCCESS = 0,
    ERROR = 1,
}

export enum ResultCodeForCaptcha {
    CAPTCHA_IS_REQUIRED = 10,
}

export type GetItemsType<T> = {
    items: Array<T>
    totalCount: number,
    error: string | null
}

export type FieldError = {
    field: string,
    error: string
}

export type APIResponseType<D = {}, RC = ResultCodes> = {
    data: D,
    resultCode: RC,
    messages: Array<string>,
    fieldsErrors?: Array<FieldError>
}

