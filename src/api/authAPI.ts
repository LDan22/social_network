import {instance, APIResponseType, ResultCodeForCaptcha, ResultCodes} from './api'

type MeResponseDataType = {
    id: number,
    email: string,
    login: string,
}

export type LoginResponseDataType = {
    userId: number,
}


const authAPI = {
    me() {
        return instance.get<APIResponseType<MeResponseDataType>>(`auth/me`).then(res => res.data)
    },

    login(email: string, password: string, rememberMe = false, captcha = null as string | null) {
        return instance.post<APIResponseType<LoginResponseDataType, ResultCodes & ResultCodeForCaptcha>>(`auth/login`, {
            email, password, rememberMe, captcha
        }).then(res => res.data)
    },

    logout() {
        return instance.delete<APIResponseType>(`auth/login`).then(res => res.data)
    }
}

export default authAPI
