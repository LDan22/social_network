import {UserApi} from '../api/rest/user-api'

const userApi = new UserApi({
    url: 'users',
    authorization: false,
    single: 'user',
    plural: 'users'
})

export const fetchUsers = async () => {
    const response = await userApi.getAll()

    return response.data
}
