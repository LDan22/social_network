import {ApiCoreImpl} from '../api-core'
import {User} from '../../model/user-model'

export class UserApi extends ApiCoreImpl<User> {
    public customRequest = () => {

    }
}
