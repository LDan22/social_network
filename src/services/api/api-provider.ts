import makeRequest from './makeRequest'

const BASE_URL = process.env.REACT_APP_API_URL

export class ApiProvider<T> {
    public getAll = (resource: string) => {
        return makeRequest({
            url: `${BASE_URL}/${resource}`,
            method: 'GET'
        })
    }

    public getSingle = (resource: string, id: number) => {
        return makeRequest({
            url: `${BASE_URL}/${resource}/${id}`,
            method: 'GET'
        })
    }

    public post = (resource: string, body: T) => {
        return makeRequest({
            url: `${BASE_URL}/${resource}`,
            data: body,
            method: 'POST'
        })
    }

    public put = (resource: string, id: number, body: T) => {
        return makeRequest({
            url: `${BASE_URL}/${resource}/${id}`,
            data: body,
            method: 'PUT'
        })
    }

    public patch = (resource: string, id: number, body: T) => {
        return makeRequest({
            url: `${BASE_URL}/${resource}/${id}`,
            data: body,
            method: 'PATCH'
        })
    }

    public delete = (resource: string, id: number) => {
        return makeRequest({
            url: `${BASE_URL}/${resource}/${id}`,
            method: 'DELETE'
        })
    }
}
