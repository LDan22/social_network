import {ApiProvider} from './api-provider'

export type ApiOptions = {
    url: string
    plural: string
    single: string
    authorization: boolean
}

interface IApiResponse {
    status: number
}

export interface IApiListResponse<T> extends IApiResponse {
    data: T[]
}

export interface IApiSingleResponse<T> extends IApiResponse {
    data: T
}

export interface ApiCore<T> {
    getAll: () => Promise<IApiListResponse<T>>
    getSingle: (id: number) => Promise<IApiSingleResponse<T>>
    post: (model: T) => Promise<IApiSingleResponse<T>>
    put: (id: number, model: T) => Promise<IApiSingleResponse<T>>
    patch: (id: number, model: T) => Promise<IApiSingleResponse<T>>
    delete: (id: number) => Promise<IApiSingleResponse<T>>
}

export class ApiCoreImpl<T> implements ApiCore<T> {
    private options: ApiOptions
    private apiProvider: ApiProvider<T>
    private authorization: boolean = false

    constructor(options: ApiOptions) {
        this.options = options
        this.authorization = options.authorization
        this.apiProvider = new ApiProvider<T>()
    }

    public getAll = (): Promise<IApiListResponse<T>> => {
        return this.apiProvider.getAll(this.options.url)
    }

    public getSingle = (id: number): Promise<IApiSingleResponse<T>> => {
        return this.apiProvider.getSingle(this.options.url, id)
    }

    public post = (model: T): Promise<IApiSingleResponse<T>> => {
        return this.apiProvider.post(this.options.url, model)
    }

    public put = (id: number, model: T): Promise<IApiSingleResponse<T>> => {
        return this.apiProvider.put(this.options.url, id, model)
    }

    public patch = (id: number, model: T): Promise<IApiSingleResponse<T>> => {
        return this.apiProvider.patch(this.options.url, id, model)
    }

    public delete = (id: number): Promise<IApiSingleResponse<T>> => {
        return this.apiProvider.delete(this.options.url, id)
    }
}
