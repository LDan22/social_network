import axios, {AxiosRequestConfig} from 'axios'
import * as config from './config'

interface RequestConfig extends AxiosRequestConfig {
    authorization?: boolean
}

const makeRequest = ({url = '/', method = 'get', params = {}, data = {}, headers = {}, authorization = false}: RequestConfig) => {
    if (headers && authorization) {
        headers.authorization = config.token
    }

    return axios({
        url, method, headers, params, data
    }).catch(err => {
        config.errorHandler(err)
        throw err
    })
}


export default makeRequest
