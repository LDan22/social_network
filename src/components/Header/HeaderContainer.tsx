import React from 'react'
import Header from './Header'
import {connect, ConnectedProps} from 'react-redux'
import {logout} from '../../redux/auth/authReducer'
import {AppStateType} from '../../redux/reduxStore'

type OwnProps = {}

export type Props = PropsFromRedux & OwnProps

class HeaderContainer extends React.Component<Props> {
    render() {
        return (
            <Header {...this.props}/>
        )
    }
}

let mapStateToProps = (state: AppStateType) => ({
    isAuth: state.auth.isAuth,
    login: state.auth.login
})

const connector = connect(mapStateToProps, {logout})

type PropsFromRedux = ConnectedProps<typeof connector>

export default connector(HeaderContainer)
