import React from 'react'
import classes from './Header.module.css'
import {NavLink} from 'react-router-dom'
import {Props} from './HeaderContainer'
import ico from '../../assets/logoWeb.png'
import text from '../../assets/textWeb.png'
import {Col, Layout} from 'antd'

const HeaderContainer = Layout.Header

const Header: React.FC<Props> = (props) => {

    return (
        <HeaderContainer  className={classes.header}>
            <Col span={16} offset={4} className={classes.content}>
                <div className={classes.logo}>
                    <img src={ico} alt={'meet me logo'} className={classes.logoImg}/>
                    <img src={text} alt={'meet me text'} className={classes.logoText}/>
                </div>
                {props.isAuth ?
                    <div>
                        <div>
                            {props.login}
                        </div>
                    </div>
                    :
                    <div className={classes.loginBlock}>
                        <NavLink to={'/login'}>
                            Login
                        </NavLink>
                    </div>
                }
            </Col>

        </HeaderContainer>

    )
}

export default Header
