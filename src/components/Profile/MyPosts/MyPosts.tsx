import React from 'react'

import classes from './MyPosts.module.css'
import Post from './Post/Post'
import {PostType, ProfileType} from '../../../types/types'
import AddNewPost, {AddPostFormValuesType} from './AddPostForm/AddPostForm'


export type MapPropsType = {
    posts: Array<PostType>
}
export type DispatchPropsType = {
    addPost: (newPostText: string) => void
}

type OwnProps = {
    profile: ProfileType
}


const MyPosts: React.FC<MapPropsType & DispatchPropsType & OwnProps> = (props) => {

    let postElements = props.posts
        .map(post =>
            <Post
                key={post.id}
                message={post.message}
                added={post.added}
                fullName={props.profile.fullName}
                avatarURL={'https://www.shareicon.net/data/512x512/2016/07/26/802043_man_512x512.png'}
                likesCount={post.likesCount}
            />
        )

    let addNewPost = (values: AddPostFormValuesType) => {
        props.addPost(values.newPostText)
    }

    return (
        <div className={classes.postsBlock}>
            <AddNewPost onSubmit={addNewPost}/>
            <div className={classes.posts}>
                {postElements}
            </div>
        </div>
    )
}

const MyPostsMemorized = React.memo(MyPosts)
export default MyPostsMemorized
