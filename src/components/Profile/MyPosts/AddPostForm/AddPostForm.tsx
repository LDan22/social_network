import classes from "../MyPosts.module.css";
import {Field, InjectedFormProps, reduxForm} from "redux-form";
import {Textarea} from "../../../common/FormsControls/FormsControls";
import React from "react";
import {maxLengthCreator, required} from "../../../../utils/validators/validators";

const maxLength10 = maxLengthCreator(10);

type PropsType = {}

export type AddPostFormValuesType = {
    newPostText: string
}
const AddNewPost: React.FC<InjectedFormProps<AddPostFormValuesType, PropsType> & PropsType> =
    (props) => {
        return (
            <form className={classes.addPost} onSubmit={props.handleSubmit}>
                <div>
                    <Field
                        component={Textarea}
                        name={'newPostText'}
                        placeholder={'New post'}
                        validate={[required, maxLength10]}
                        className={classes.postInput}
                    />
                </div>
                <div>
                    <button className={classes.postBtn}>
                        Add post
                    </button>
                </div>
            </form>
        )
    }

export default reduxForm<AddPostFormValuesType, PropsType>({form: 'ProfileAddNewPost'})(AddNewPost)
