import React from 'react';
import classes from "./Post.module.css"
import {AiOutlineLike} from "react-icons/ai";

type PropsType = {
    avatarURL: string,
    fullName: string,
    added: string,
    message: string,
    likesCount: number
}

const Post: React.FC<PropsType> = (props) => {
    return (
        <div className={classes.item}>
            <div>
                <img
                    src={props.avatarURL}
                    alt={'avatar'}
                />
            </div>
            <div className={classes.postInfo}>
                <div>
                    <div className={classes.name}>
                        @{props.fullName}
                    </div>
                    <div className={classes.date}>
                        {props.added}
                    </div>
                </div>

                <div className={classes.message}>
                    {props.message}
                </div>

                <div className={classes.postActions}>
                    <div className={classes.action}>
                        <div className={classes.likeBtn}><AiOutlineLike/></div>
                        <div className={classes.likeCount}>{props.likesCount}</div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Post
