import React, {useState} from 'react'
import SocialMediaIcon from '../../../common/SocialMediaIcon'
import {FaAngleDown, FaAngleUp} from 'react-icons/fa'
import styles from './ProfileData.module.css'
import {AiOutlineCheckCircle, ImCancelCircle} from 'react-icons/all'
import {ContactsType, ProfileType} from '../../../../types/types'

type Props = {
    profile: ProfileType,
    isOwner: boolean,
    activateEditMode: () => void
}

const ProfileData: React.FC<Props> = ({profile, isOwner, activateEditMode}) => {
    const [showMore, setShowMore] = useState(false)

    return (
        <div className={styles.descriptionBlock}>
            {isOwner &&
            <div className={styles.editBtn}>
                <button onClick={activateEditMode}>
                    Edit
                </button>
            </div>
            }

            {!showMore ?
                <div onClick={() => setShowMore(true)} className={styles.showMore}>
                    <div>
                        More
                    </div>
                    <FaAngleDown/>
                </div>
                :
                <div onClick={() => setShowMore(false)} className={styles.showMore}>
                    <div>
                        Hide
                    </div>
                    <FaAngleUp/>
                </div>
            }

            {showMore &&
            <div className={styles.profileInfo}>
                <div className={styles.inline}>
                    <div>
                        Looking for a job:
                    </div>
                    <div className={styles.icon}>
                        {profile.lookingForAJob ? <AiOutlineCheckCircle/> : <ImCancelCircle/>}
                    </div>
                </div>
                <div>
                    My professional skills: {profile.lookingForAJobDescription}
                </div>

                <div>
                    About me: {profile.aboutMe}
                </div>
                <div>
                    <div className={styles.contacts}>
                        {Object.keys(profile.contacts).map((key) => {
                                return <Contact contactTitle={key}
                                                contactValue={profile.contacts[key as keyof ContactsType]} key={key}/>
                            }
                        )}
                    </div>
                </div>
            </div>
            }
        </div>
    )
}


type ContactProps = {
    contactTitle: string,
    contactValue: string,
}

const Contact: React.FC<ContactProps> = ({contactTitle, contactValue}) => {
    return (
        <div>
            <a href={contactValue}>
                <SocialMediaIcon title={contactTitle}/>
            </a>
        </div>
    )
}

export default ProfileData
