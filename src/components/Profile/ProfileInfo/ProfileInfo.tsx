import React, {ChangeEvent, useState} from 'react'
import classes from './ProfileInfo.module.css'
import ProfileStatus from './ProfileStatus/ProfileStatus'
import ProfileDataForm from './ProfileDataForm/ProfileDataForm'
import ProfileData from './ProfileData/ProfileData'
import {ProfileType} from '../../../types/types'
import {PropsType as Props} from '../Profile'
import Preloader from '../../common/Preloader/Preloader'


const ProfileInfo: React.FC<Props> = ({profile, status, updateStatus, savePhoto, isOwner, saveProfile}) => {
    const [editMode, setEditMode] = useState(false)

    if (!profile) {
        return <Preloader/>
    }

    const onUploadAvatar = (e: ChangeEvent<HTMLInputElement>) => {
        if (e.target.files && e.target.files.length) {
            savePhoto(e.target.files[0])
        }
    }

    const onUpdateProfile = (formData: ProfileType) => {
        // todo: Remove .then()
        saveProfile(formData).then(
            () => {
                setEditMode(false)
            }
        ).catch((e) => {
            console.log(e)
        })
    }

    const cancelEditing = () => {
        setEditMode(false)
    }

    return (
        <div className={classes.profileInfo}>

            <div className={classes.descriptionBlock}>
                <div className={classes.avatar}>
                    {isOwner &&
                    <div className={classes.imageUpload}>
                        <input type={'file'} onChange={onUploadAvatar}/>
                    </div>
                    }
                    <img
                        src={profile.photos.large ? profile.photos.large : `https://html5css.ru/howto/img_avatar.png`}
                        alt={'avatar'}
                        className={classes.avatarImg}
                    />
                </div>

                <div className={classes.fullName}>
                    @{profile.fullName}
                </div>
                <div className={classes.status}>
                    <ProfileStatus status={status} updateStatus={updateStatus} isOwner={isOwner}/>

                </div>

                {editMode ?
                    <ProfileDataForm profile={profile} onSubmit={onUpdateProfile} initialValues={profile}
                                     cancelEditing={cancelEditing}/>
                    :
                    <ProfileData profile={profile} isOwner={isOwner} activateEditMode={() => setEditMode(true)}/>
                }
            </div>
        </div>
    )
}


export default ProfileInfo
