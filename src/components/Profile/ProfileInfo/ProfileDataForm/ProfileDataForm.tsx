import React from 'react'
import {FormSection, InjectedFormProps, reduxForm} from 'redux-form'
import {createField, GetStringKeys, Input, Textarea} from '../../../common/FormsControls/FormsControls'
import styles from './ProfileDataForm.module.css'
import {ContactsType, ProfileType} from '../../../../types/types'

type OwnProps = {
    profile: ProfileType,
    cancelEditing: () => void
}

type ProfileDataFormKeys = GetStringKeys<ProfileType>


const ProfileDataForm: React.FC<InjectedFormProps<ProfileType, OwnProps> & OwnProps> =
    ({handleSubmit, profile, error, cancelEditing}) => {
        return (
            <form onSubmit={handleSubmit} className={styles.form}>
                <div className={styles.buttons}>
                    <button className={styles.actionBtn}>
                        Save
                    </button>
                    <button onClick={cancelEditing} className={styles.actionBtn}>
                        Cancel
                    </button>
                </div>
                {error &&
                <div>
                    {error}
                </div>
                }

                {createField<ProfileDataFormKeys>('Full name', 'fullName', [], Input)}


                <div>
                    {createField<ProfileDataFormKeys>('Full name', 'fullName', [], Input)}
                </div>
                <div>
                    {createField<ProfileDataFormKeys>(undefined, 'lookingForAJob', [], Input,
                        {type: 'checkbox'}, 'Looking for a job')}

                </div>
                <div className={styles.textareaField}>
                <span>
                    My skills
                </span>
                    {createField<ProfileDataFormKeys>('Skills', 'lookingForAJobDescription', [], Textarea)}

                </div>
                <div className={styles.textareaField}>
                 <span>
                   About me
                </span>
                    {createField<ProfileDataFormKeys>('About me', 'aboutMe', [], Textarea)}

                </div>

                <FormSection name={'contacts'} initialValues={profile.contacts}>
                    <ContactsForm profile={profile}/>
                </FormSection>


            </form>
        )
    }


type ContactsProps = {
    profile: ProfileType
}

type ContactsKeys = GetStringKeys<ContactsType>

const ContactsForm: React.FC<ContactsProps> = ({profile}) => {
    return (
        <div>
            <div>
                Contacts:
            </div>
            {Object.keys(profile.contacts).map(key => (
                <div key={key}>
                    {createField<ContactsKeys>(key, key as keyof ContactsType, [], Input)}
                </div>
            ))}
        </div>
    )
}


export default reduxForm<ProfileType, OwnProps>({form: 'profileDataForm'})(ProfileDataForm)
