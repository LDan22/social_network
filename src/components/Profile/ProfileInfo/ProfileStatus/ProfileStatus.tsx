import React, {ChangeEvent, useEffect, useState} from 'react'

type Props = {
    status: string,
    updateStatus: (status: string) => void,
    isOwner: boolean
}

const ProfileStatus: React.FC<Props> = (props) => {

    let [editMode, setEditMode] = useState(false)
    let [status, setStatus] = useState(props.status)

    useEffect(() => {
        setStatus(props.status)
    }, [props.status])

    const onActivateEditMode = () => {
        if (props.isOwner)
            setEditMode(true)
    }

    const onDeactivateEditMode = () => {
        setEditMode(false)
        if (status !== props.status)
            props.updateStatus(status)
    }

    const onStatusChange = (e: ChangeEvent<HTMLInputElement>) => {
        let {value} = e.target
        setStatus(value)
    }

    return (
        <div>
            {!editMode &&
            <div onDoubleClick={onActivateEditMode}>
                {props.status || 'No status'}
            </div>
            }

            {editMode &&
            <div>
                <input
                    value={status}
                    onChange={onStatusChange}
                    autoFocus
                    onBlur={onDeactivateEditMode}
                />
            </div>
            }

        </div>
    )
}

export default ProfileStatus
