import React, {ChangeEvent} from 'react'

type Props = {
    status: string,
    updateStatus: (status: string) => void,
    isOwner: boolean
}

type State = {
    editMode: boolean,
    status: string
}

class ProfileStatus extends React.Component<Props, State> {
    state = {
        editMode: false,
        status: this.props.status
    }
    onActivateEditMode = () => {
        this.setState({
            editMode: true
        })
    }

    onDeactivateEditMode = () => {
        this.setState({
            editMode: false
        })
        this.props.updateStatus(this.state.status)
    }

    onStatusChange = (e: ChangeEvent<HTMLInputElement>) => {
        this.setState({
            status: e.target.value
        })
    }

    render() {
        let {editMode, status} = this.state

        return (
            <div>
                {!editMode &&
                <div onDoubleClick={this.onActivateEditMode} className={'status'}>
                    {this.props.status || 'No status'}
                </div>
                }

                {editMode &&
                <div>
                    <input
                        value={status}
                        onChange={this.onStatusChange}
                        autoFocus
                        onBlur={this.onDeactivateEditMode}
                    />
                </div>
                }

            </div>
        )
    }
}

export default ProfileStatus
