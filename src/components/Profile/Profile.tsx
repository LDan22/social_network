import React from 'react'

import ProfileInfo from './ProfileInfo/ProfileInfo'
import MyPostsContainer from './MyPosts/MyPostsContainer'
import classes from './Profile.module.css'
import Preloader from '../common/Preloader/Preloader'
import {ProfileType} from '../../types/types'

export type PropsType = {
    profile: ProfileType | null,
    isOwner: boolean,
    status: string,
    updateStatus: (status: string) => void
    savePhoto: (photo: File) => void,
    saveProfile: (profile: ProfileType) => Promise<any>
}

const Profile: React.FC<PropsType> = (props) => {

    if (!props.profile) {
        console.log('profile is null')
        return (
            <Preloader/>
        )
    }

    return (
        <div>
            <div className={classes.profileContainer}>
                <ProfileInfo profile={props.profile} status={props.status} updateStatus={props.updateStatus}
                             isOwner={props.isOwner} savePhoto={props.savePhoto} saveProfile={props.saveProfile}/>
                <MyPostsContainer profile={props.profile}/>
            </div>

        </div>
    )
}

export default Profile
