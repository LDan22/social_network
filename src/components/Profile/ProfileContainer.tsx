import React from 'react'

import Profile from './Profile'
import {connect, ConnectedProps} from 'react-redux'
import {getUserProfile, getUserStatus, savePhoto, updateProfile, updateStatus} from '../../redux/profile/profileReducer'
import {Redirect, RouteComponentProps, withRouter} from 'react-router-dom'
import {compose} from 'redux'
import {AppStateType} from '../../redux/reduxStore'
import {ProfileType} from '../../types/types'

type RouterParamsProps = {
    userId: string
}

type OwnProps = {}

type PropsType = PropsFromRedux & OwnProps & RouteComponentProps<RouterParamsProps>

type StateType = {
    push: boolean
}

class ProfileContainer extends React.Component<PropsType, StateType> {
    state = {
        push: false
    }

    getUserId = () => {
        let userId = parseInt(this.props.match.params.userId)
        if (!userId) {
            if (this.props.userId)
                userId = this.props.userId
        }
        return userId
    }

    refreshProfile = () => {
        let userId = this.getUserId()
        if (!userId) {
            this.setState({
                push: true
            })
        } else {
            this.props.getUserProfile(userId)
            this.props.getUserStatus(userId)
        }
    }

    componentDidMount() {
        this.refreshProfile()
    }

    componentDidUpdate(prevProps: PropsType, prevState: StateType) {
        if (prevProps.match.params.userId !== this.props.match.params.userId) {
            this.refreshProfile()
        }
    }

    saveProfile = (formData: ProfileType) => {
        return this.props.updateProfile({...this.props.profile, ...formData})
    }

    render() {
        if (this.state.push) {
            console.log('redirect')
            return <Redirect to={'/login'}/>
        }
        const isOwner: boolean = this.props.userId !== null && this.props.match.params.userId === undefined

        return (
            <Profile {...this.props}
                     profile={this.props.profile}
                     status={this.props.status}
                     updateStatus={this.props.updateStatus}
                     isOwner={isOwner}
                     savePhoto={this.props.savePhoto}
                     saveProfile={this.saveProfile}
            />
        )
    }
}

let mapStateToProps = (state: AppStateType) => ({
    profile: state.profilePage.profile,
    userId: state.auth.userId,
    status: state.profilePage.status,
    isAuth: state.auth.isAuth
})

const connector = connect(mapStateToProps, {
    getUserProfile,
    getUserStatus,
    updateStatus,
    savePhoto,
    updateProfile
})

type PropsFromRedux = ConnectedProps<typeof connector>

export default compose<React.ComponentType>(
    connector,
    withRouter
)(ProfileContainer)
