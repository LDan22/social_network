import React from 'react'
import Paginator from './Paginator'
import {create} from 'react-test-renderer'
import styles from './Paginator.module.css'


describe('Paginator test', () => {
    test('should be 1 selected page', () => {
        const component = create(<Paginator currentPage={1} totalItemsCount={11} pageSize={1} onPageChanged={() => {
        }}/>)
        const root = component.root
        let selectedPages = root.findAllByProps({className: styles.paginationBtn + ' ' + styles.selectedPage})
        expect(selectedPages.length).toBe(1)
    })

    test('page count is 11 but should be visible 10 where 1 is selected and 9 not', () => {
        const component = create(<Paginator currentPage={1} totalItemsCount={11} pageSize={1} onPageChanged={() => {
        }}/>)
        const root = component.root
        let notSelectedPages = root.findAllByProps({className: styles.paginationBtn})
        expect(notSelectedPages.length).toBe(9)
    })
})
