import React, {useState} from 'react'

import styles from './Paginator.module.css'
import {FaAngleDoubleLeft, FaAngleDoubleRight, FaAngleLeft, FaAngleRight} from 'react-icons/fa'
import cn from 'classnames'

type PropsType = {
    currentPage: number,
    totalItemsCount: number,
    pageSize: number
    portionSize?: number,
    onPageChanged: (page: number) => void
}

let Paginator: React.FC<PropsType> = ({
                                          currentPage, totalItemsCount, pageSize,
                                          onPageChanged, portionSize = 10
                                      }) => {

    let [portionNumber, setPortionNumber] = useState<number>(Math.ceil(currentPage / portionSize))

    let pagesCount = Math.ceil(totalItemsCount / pageSize)
    let pages: Array<number> = []
    for (let i = 1; i <= pagesCount; i++) {
        pages.push(i)
    }

    let portionCount = Math.ceil(pagesCount / portionSize)
    let leftPortionPageNumber = (portionNumber - 1) * portionSize + 1
    let rightPortionPageNumber = portionNumber * portionSize

    return (
        <div className={styles.paginator}>
            <div>
                <button onClick={() => setPortionNumber(1)} disabled={portionNumber <= 1}
                        className={styles.arrowBtn}>
                    <FaAngleDoubleLeft/>
                </button>
                <button onClick={() => setPortionNumber(portionNumber - 1)} disabled={portionNumber <= 1}
                        className={styles.arrowBtn}>
                    <FaAngleLeft/>
                </button>
            </div>

            {pages.filter(p => p >= leftPortionPageNumber && p <= rightPortionPageNumber).map(p => (
                    <div
                        className={cn(styles.paginationBtn, {[styles.selectedPage]: p === currentPage})}
                        key={p}
                        onClick={(e) => onPageChanged(p)}
                    >
                        {p}
                    </div>
                )
            )}

            <div>
                <button onClick={() => setPortionNumber(portionNumber + 1)} disabled={portionCount <= portionNumber}
                        className={styles.arrowBtn}>
                    <FaAngleRight/>
                </button>
                <button onClick={() => setPortionNumber(portionCount)} disabled={portionCount <= portionNumber}
                        className={styles.arrowBtn}>
                    <FaAngleDoubleRight/>
                </button>
            </div>
        </div>
    );
}

export default Paginator
