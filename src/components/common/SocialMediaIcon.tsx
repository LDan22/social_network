import React from 'react'
import {FaFacebook, FaGithub, FaGoogle, FaInstagram, FaTwitter, FaYoutube} from 'react-icons/fa'

type Props = {
    title: string
}

let SocialMediaIcon: React.FC<Props> = ({title}) => {
    const getIcon = (title: string) => {
        switch (title.toLowerCase()) {
            case 'facebook':
                return <FaFacebook/>
            case 'google':
                return <FaGoogle/>
            case 'twitter':
                return <FaTwitter/>
            case 'instagram':
                return <FaInstagram/>
            case 'github':
                return <FaGithub/>
            case 'youtube':
                return <FaYoutube/>
            default:
                return <div>{title}</div>
        }
    }

    const icon = getIcon(title)
    return (
        <div style={{color: '#e6e6e6'}}>
            {icon}
        </div>
    )

}

export default SocialMediaIcon
