import React from 'react';
import loader from "../../../assets/Eclipse-1s-200px.svg";
import styles from './Preloader.module.css'

let Preloader = (props) => {
    return (
        <div className={styles.preloader}>
            <img src={loader} alt={'Loading...'}/>
        </div>
    )
}

export default Preloader;
