import React from 'react'

import classes from '../Dialogs.module.css'

type Props = {
    message: string,
    id: number
}

const Message: React.FC<Props> = (props) => {
    return (
        <div className={classes.message}>
            {props.message}
        </div>
    )
}


export default Message
