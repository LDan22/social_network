import React, {useEffect, useRef} from 'react'

import classes from './Dialogs.module.css'
import DialogItem from './DialogItem/DialogItem'
import Message from './Message/Message'
import AddMessageForm from './SendMessageForm/SendMessageForm'
import {InitialStateType as DialogsPageState} from '../../redux/dialogs/dialogsReducer'


type PropsType = {
    state: DialogsPageState,
    addMessage: (text: string) => void
}

export type NewMessageFormType = {
    newMessageText: string
}

const Dialogs: React.FC<PropsType> = (props) => {

    let dialogsElements =
        props.state.dialogs.map(dialog => <DialogItem key={dialog.id} name={dialog.name} id={dialog.id}/>)

    let messageElements =
        props.state.messages.map(data => <Message key={data.id} id={data.id} message={data.message}/>)

    const chatContainer = useRef<HTMLDivElement>(null)

    useEffect(() => {
        if (chatContainer.current) {
            const scroll =
                chatContainer.current.scrollHeight -
                chatContainer.current.clientHeight

            chatContainer.current.scrollTo(0, scroll)
        }
    }, [props.state.messages])


    let addNewMessage = (values: NewMessageFormType) => {
        props.addMessage(values.newMessageText)
    }

    return (
        <div className={classes.dialogs}>
            <div className={classes.dialogsItems}>
                {dialogsElements}
            </div>

            <div className={classes.messages}>
                <div className={classes.messagesLayout} ref={chatContainer}>
                    {messageElements}
                </div>

                <AddMessageForm onSubmit={addNewMessage}/>
            </div>
        </div>
    )
}

export default Dialogs
