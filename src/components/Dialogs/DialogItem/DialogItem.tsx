import React from 'react'

import classes from '../Dialogs.module.css'
import {NavLink} from 'react-router-dom'

type Props = {
    name: string,
    id: number,
}

const DialogItem: React.FC<Props> = (props) => {
    return (
        <NavLink to={`/dialogs/${props.id}`} activeClassName={classes.active}>
            <div className={classes.dialog}>
                {props.name}
            </div>
        </NavLink>

    )
}

export default DialogItem
