import React from 'react'
import classes from '../Dialogs.module.css'
import {InjectedFormProps, reduxForm} from 'redux-form'
import {createField, GetStringKeys, Input} from '../../common/FormsControls/FormsControls'
import {maxLengthCreator, required} from '../../../utils/validators/validators'
import {NewMessageFormType} from '../Dialogs'

let maxLength100 = maxLengthCreator(100)

type AddMessageKeys = GetStringKeys<NewMessageFormType>

const AddMessageForm: React.FC<InjectedFormProps<NewMessageFormType>> = (props) => {
    return (
        <form className={classes.addMessage} onSubmit={props.handleSubmit}>
            <div className={classes.messageInput}
            >
                {createField<AddMessageKeys>('New message ...', 'newMessageText', [required, maxLength100], Input)}

            </div>
            <div className={classes.btn}>
                <button className={classes.addBtn}>Send</button>
            </div>
        </form>
    )
}

export default reduxForm<NewMessageFormType>({form: 'dialogAddMessageForm'})(AddMessageForm)
