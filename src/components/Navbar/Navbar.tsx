import React from 'react'

import classes from './Navbar.module.css'
import {NavLink} from 'react-router-dom'
import cn from 'classnames'
import {AiOutlineWechat, AiOutlineMessage, BsSearch, FaRegUser} from 'react-icons/all'
import {Layout} from 'antd'

type PropsType = {
    isAuth: boolean,
    logout: () => void
}

const {Sider} = Layout

const Navbar: React.FC<PropsType> = (props) => {
    return (
        <Sider className={classes.nav}>
            <div className={classes.fixedNav}>
                <NavLink to={'/profile'} activeClassName={classes.active}>
                    <div className={classes.item}>
                        <FaRegUser/> Profile
                    </div>
                </NavLink>
                <NavLink to={'/dialogs'} activeClassName={classes.active}>
                    <div className={classes.item}>
                        <AiOutlineMessage/> Messages
                    </div>
                </NavLink>
                <NavLink to={'/users'} activeClassName={classes.active}>
                    <div className={classes.item}>
                        <BsSearch/> Find Users
                    </div>
                </NavLink>
                <NavLink to={'/chat'} activeClassName={classes.active}>
                    <div className={classes.item}>
                        <AiOutlineWechat/> Chat
                    </div>
                </NavLink>

                {props.isAuth &&
                <div className={cn(classes.logout, classes.item)} onClick={props.logout}>
                    Logout
                </div>
                }
            </div>
        </Sider>
    )
}

export default Navbar
