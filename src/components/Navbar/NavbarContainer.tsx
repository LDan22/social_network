import React from 'react'
import {compose} from "redux";
import {connect} from "react-redux";
import {logout} from "../../redux/auth/authReducer";
import Navbar from "./Navbar";
import {AppStateType} from "../../redux/reduxStore";


type MapStatePropsType = {
    isAuth: boolean,

}
type MapDispatchPropsType = {
    logout: () => void

}

type PropsType = MapDispatchPropsType & MapStatePropsType

class NavbarContainer extends React.Component<PropsType> {
    render() {
        return (
            <Navbar isAuth={this.props.isAuth} logout={this.props.logout}/>
        );
    }
}


let mapStateToProps = (state: AppStateType) => ({
    isAuth: state.auth.isAuth,
})

export default compose(
    connect<MapStatePropsType, MapDispatchPropsType, {}, AppStateType>(mapStateToProps, {
        logout
    })
)(NavbarContainer)
