import React from 'react'
import {Users} from './Users'


type PropsType = {}

const UsersPage: React.FC<PropsType> = () => {
    return (
        <div>
            <Users/>
        </div>

    )
}

export default UsersPage
