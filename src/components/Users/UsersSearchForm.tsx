import {Field, Form, Formik} from 'formik'
import React from 'react'
import {FilterType} from '../../redux/users/usersReducer'
import {useSelector} from 'react-redux'
import {getUsersFilter} from '../../redux/users/usersSelectors'

const usersSearchFormValidate = (values: any) => {
    const errors = {}

    return errors
}

type PropsType = {
    onFilterChanged: (filter: FilterType) => void,
}

type FriendFormType = 'null' | 'true' | 'false'
type ValuesType = {
    term: string,
    friend: FriendFormType
}


export const UsersSearchForm: React.FC<PropsType> = React.memo(({onFilterChanged}) => {
    const filter = useSelector(getUsersFilter)

    const submit = (values: ValuesType, {setSubmitting}: { setSubmitting: (isSubmitting: boolean) => void }) => {
        const newFilter = {
            term: values.term,
            friend: values.friend === 'null' ? null : values.friend === 'true'
        }
        // TODO: set submit false after search users is resolved
        onFilterChanged(newFilter)
        setSubmitting(false)
    }

    return (
        <div>
            <Formik
                enableReinitialize
                initialValues={{term: filter.term, friend: String(filter.friend) as FriendFormType} as ValuesType}
                validate={usersSearchFormValidate}
                onSubmit={submit}
            >
                {({isSubmitting}) => (
                    <Form>
                        <Field type="text" name="term"/>
                        <Field as="select" name="friend">
                            <option value="null">All</option>
                            <option value="true">Followed</option>
                            <option value="false">Unfollowed</option>
                        </Field>
                        <button type="submit" disabled={isSubmitting}>
                            Find
                        </button>
                    </Form>
                )}
            </Formik>
        </div>
    )
})
