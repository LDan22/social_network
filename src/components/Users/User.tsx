import React from 'react'

import classes from './Users.module.css'
import {defaultUserPhoto} from '../../assets/images'
import {NavLink} from 'react-router-dom'
import {UserType} from '../../types/types'

type PropsType = {
    user: UserType,
    followingInProgress: Array<number>,
    follow: (userId: number) => void,
    unfollow: (userId: number) => void,
    isAuth: boolean
}

let User: React.FC<PropsType> = ({user, followingInProgress, follow, unfollow, isAuth}) => {
    return (
        <div className={classes.userRow}>
            <div className={classes.userInfo}>
                <div className={classes.userPhoto}>
                    <NavLink to={`/profile/${user.id}`}>
                        <img
                            src={user.photos.small ? user.photos.small : defaultUserPhoto}
                            alt={'user avatar'}
                        />
                    </NavLink>
                </div>
                <div className={classes.userData}>
                    <div className={classes.textBold}>
                        {user.name}
                    </div>
                    <div className={classes.userInfoLocation}>
                        <div>
                            Country,
                        </div>
                        <div>
                            City
                        </div>
                    </div>
                    <div className={classes.userStatus}>
                        {user.status}
                    </div>
                </div>
            </div>


            {isAuth &&
            <div>
                {user.followed ?
                    <button className={classes.toggleFollowBtn}
                            onClick={() => {
                                unfollow(user.id)
                            }}
                            disabled={followingInProgress.some(id => id === user.id)}
                    >
                        Unfollow
                    </button>
                    :
                    <button className={classes.toggleFollowBtn}
                            onClick={() => {
                                follow(user.id)
                            }}
                            disabled={followingInProgress.some(id => id === user.id)}
                    >
                        Follow
                    </button>
                }
            </div>
            }
        </div>
    )
}

export default User
