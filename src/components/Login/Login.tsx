import React from 'react'
import {InjectedFormProps, reduxForm} from 'redux-form'
import {useDispatch, useSelector} from 'react-redux'
import {login} from '../../redux/auth/authReducer'
import {Redirect} from 'react-router-dom'
import {createField, GetStringKeys, Input} from '../common/FormsControls/FormsControls'
import formStyles from '../common/FormsControls/FormsControl.module.css'
import {required} from '../../utils/validators/validators'
import styles from './Login.module.css'
import {AppStateType} from '../../redux/reduxStore'
import {Button, Space} from 'antd'

type LoginFormOwnProps = {
    captchaUrl: string | null
}

type LoginBodyType = {
    email: string,
    password: string,
    rememberMe: boolean,
    captcha: string
}

type LoginFormValuesTypeKeys = GetStringKeys<LoginBodyType>

const LoginForm: React.FC<InjectedFormProps<LoginBodyType, LoginFormOwnProps> & LoginFormOwnProps> =
    ({handleSubmit, error, captchaUrl}) => {
        return (
            <form onSubmit={handleSubmit}>
                <Space direction={'vertical'}>
                    <div>
                        <div className={styles.label}>Email:</div>
                        {createField<LoginFormValuesTypeKeys>('Email', 'email', [required], Input)}
                    </div>

                    <div>
                        <div className={styles.label}>Password:</div>
                        {createField<LoginFormValuesTypeKeys>('Password', 'password', [required],
                            Input, {type: 'password'})}
                    </div>

                    <div>
                        {createField<LoginFormValuesTypeKeys>(undefined, 'rememberMe', [],
                            Input, {type: 'checkbox'}, 'Remember me')}
                    </div>

                    {captchaUrl &&
                    <div>
                        <img src={captchaUrl} alt={'captcha'}/>
                        {captchaUrl && createField<LoginFormValuesTypeKeys>('Symbols from image', 'captcha',
                            [required], Input, {})}
                    </div>
                    }
                    {error &&
                    <div className={formStyles.formError}>
                        {error}
                    </div>
                    }
                    <div className={styles.loginBtn}>
                        <Button block htmlType="submit">
                            Login
                        </Button>
                    </div>
                </Space>
            </form>
        )
    }

const LoginReduxForm = reduxForm<LoginBodyType, LoginFormOwnProps>({form: 'login'})(LoginForm)

const Login: React.FC = () => {
    const captchaUrl = useSelector((state: AppStateType) => state.auth.captchaUrl)
    const isAuth = useSelector((state: AppStateType) => state.auth.isAuth)
    const dispatch = useDispatch()

    const onSubmit = (formData: LoginBodyType) => {
        dispatch(login(formData.email, formData.password, formData.rememberMe, formData.captcha))
    }

    if (isAuth) {
        return <Redirect to={'/profile'}/>
    }

    return (
        <div className={styles.login}>
            <h2 style={{color: '#e6e6e6'}}>Login</h2>
            <LoginReduxForm onSubmit={onSubmit} captchaUrl={captchaUrl}/>
        </div>
    )
}

export default Login
