import {Redirect} from 'react-router-dom'
import React from 'react'
import {connect} from 'react-redux'
import {AppStateType} from '../redux/reduxStore'

type PropsType = {
    isAuth: boolean
}

let mapStateToProps = (state: AppStateType) => ({
    isAuth: state.auth.isAuth
} as PropsType)


export function withAuthRedirect<WCP>(WrappedComponent: React.ComponentType<WCP>) {

    const RedirectComponent: React.FC<PropsType> = (props) => {
        let {isAuth, ...restProps} = props

        if (!isAuth) return <Redirect to={'/login'}/>

        return <WrappedComponent {...restProps as WCP}/>
    }

    return connect<PropsType, {}, WCP, AppStateType>(mapStateToProps)(RedirectComponent)
}
